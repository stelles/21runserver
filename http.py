#!/usr/bin/python
from BaseHTTPServer import BaseHTTPRequestHandler,HTTPServer
import urlparse
import cgi
from SocketServer import ThreadingMixIn
import threading

import urllib2
import string
import json
import datetime
import random   

PORT_NUMBER = 8081

# SETTING UP DUMMY DATA: for the datetimes of upcoming games
game_days = dict()
game_days_list = list()

# Users need IDs, while we are not using the DB, User_ID will be count of users
# when user is added.
user_count = 0

def setup_game_days():
    count = 0
    days_ahead = 0
    while(count < 7):
        rand = random.randint(0, 10)
        if rand > 4:
            date = datetime.datetime.today()
            date += datetime.timedelta(days=days_ahead + count)

            game_days[count] = date.strftime("%Y-%m-%d")
            game_days_list.append(date.strftime("%Y-%m-%d"))
            count += 1
        else:
            days_ahead += 1

setup_game_days()

class User:
    def __init__(self, username, password):
        self.username = username
        self.password = password
        self.id = 0
        self.name = ""
        self.email = ""
        self.gender = ""
        self.dob = datetime.datetime.today()
        self.favorite_team = ""
        self.picks = dict()


    def setName(self, name):
        self.name = name

    def setID(self, userid):
        self.id = userid

    def setEmail(self, email):
        self.email = email

    def setGender(self, gender):
        self.gender = gender

    def setFavoriteTeam(self, favorite_team):
        self.favorite_team = favorite_team

    def setDOB(self, dob):
        self.dob = dob

    def setupEmptyPicks(self, days):
        # Get User's picks from the database for this list of days here:
        for day in days:
            self.picks[day] = ["", ""]

    def updateData(self, args):
        for key in args:
            if key == "name":
                self.name = args[key]
            elif key == "email":
                self.email = args[key]
            elif key == "gender":
                self.gender = args[key]
            elif key == "favorite_team":
                self.favorite_team = args[key]
            elif key == "date_of_birth":
                self.dob = datetime.datetime.strptime( args[key], "%Y-%m-%d" )

    def jsonify(self):
        #TODO
        values = dict()
        values["username"] = self.username
        values["name"] = self.name
        values["email"] = self.email
        values["gender"] = self.gender
        values["favorite_team"] = self.favorite_team
        values["dob"] = self.dob.strftime("%Y-%m-%d")
        return json.dumps(values)

    def jsonify_picks(self):
        return json.dumps(self.picks)

user_count += 1
# TEST USERS:
user1 = User("brocksampson", "kelly")
user1.setName("Brock Sampson")
user1.setID(user_count)
user1.setEmail("bsamp@gmail.com")
user1.setGender("male")
user1.setFavoriteTeam("Chucky Cheese")
user1.setDOB(datetime.datetime.today())
user1.setupEmptyPicks(game_days_list)
user1.picks[game_days_list[0]] = ["Lionel Messi", "Luis Suarez"]

user_count += 1

user2 = User("ainsley", "ainsley")
user2.setID(user_count)

TEST_USERS = [user1, user2]

def get_user(name):
    for user in TEST_USERS:
        print user.username + " ?= " + name
        if user.username == name:
            return user
    return 0

def update_player_selections(user, selections):
    if user in TEST_USERS:
        TEST_USERS.remove(user)
        user.picks = selections
        TEST_USERS.append(user)

# Static List of Leagues for Dummy Data currently.
leagues = dict()
leagues["0"] = "Premier League"
leagues["1"] = "Bundesliga"
leagues["2"] = "Ligue 1"
leagues["3"] = "La Liga"
leagues["4"] = "Serie A"

teams = dict()
teams[leagues["0"]] = ["Arsenal", "Manchester United", "Chelsea", "Everton", "Liverpool"]
teams[leagues["1"]] = ["Bayern Munich", "Bayer Leverkusen", "Borussia Dortmund"]
teams[leagues["2"]] = ["AS Monaco", "Lille", "Stade Rennais"]
teams[leagues["3"]] = ["Athletic Bilbao", "Atletico Madrid", "Barcelona", "Real Madrid"]
teams[leagues["4"]] = ["AC Milan", "Fiorentina Inter Milan", "Juventus", "Napoli"]

players = dict()
# Premier League - Each Teams Player Dummy Data
players["Arsenal"] = [ "Mesut Ozil", "Santi Cazorla", "Jack Wilshere", "Theo Walcott"]
players["Manchester United"] = [ "Wayne Rooney", "Juan Mata", "Robin van Persie", "David de Gea", "Adnan Januzaj"]
players["Chelsea"] = [ "Eden Hazard", "Oscar", "Petr Cech"]
players["Everton"] = [ "Romelu Lukaku", "Kevin Mirallas", "Steven Naismith", "Christian Atsu"]
players["Liverpool"] = []
# Bundesliga - Each Teams Player Dummy Data
players["Bayern Munich"] = [ "Arjen Robben", "Franch Ribery", "Xabi Alonso", "Holger Badstuber"]
players["Bayer Leverkusen"] = [ "Karim Bellarabi", "Lars Bender", "Omer Toprak", "Emir Spahic", "Roberto Hilbert"]
players["Borussia Dortmund"] = [ "Marco Reus", "Mats Hummels", "Nuri Sahin", "Marcel Schmelzer"]
# Ligue 1 - Each Teams Player Dummy Data
players["AS Monaco"] = [ "Layvin Kurzawa", "Tiemoue Bakayoko", "Jeremey Toulalan", "Fabinho", "Wallace"]
players["Lille"] = [ "Simon Kjaer", "Idrissa Gueye", "Djibril Sidibe", "Marko Basa"]
players["Stade Rennais"] = [ "Mexer", "Sylvain Armand", "Paul-Georges Ntep", "Cheikh M'Bengue"]
# La Liga - Each Teams Player Dummy Data
players["Athletic Bilbao"] = [ "Aritz Aduriz", "Ander Iturraspe", "Aymeric Laporte", "Mikel San Jose"]
players["Atletico Madrid"] = [ "Tiago", "Gimenez", "Mario Mandzukic", "Diego Godin"]
players["Barcelona"] = [ "Lionel Messi", "Neymar", "Luis Suarez", "Martin Montoya", "Gerard Pique", "Marc Bartra"]
players["Real Madrid"] = [ "Cristiano Ronaldo", "James Rodriguez", "Gareth Bale", "Luka Modric"]
# Serie A - Each Teams Player Dummy Data
players["AC Milan"] = [ "Jeremy Menez", "Luca Antonelli", "Nigel de Jong", "Giacomo Bonaventura", "Keisuke Honda"]
players["Fiorentina Inter Milan"] = [ "Fredy Guarin", "Davide Santon", "Andrea Ranocchia", "Juan Pablo Carrizo"]
players["Juventus"] = [ "Paul Pogba", "Carlos Tevez", "Andrea Pirlo", "Giorgio Chiellini", "Arturo Vidal"]
players["Napoli"] = [ "Faouzi Ghoulam", "Kalidou Koulibaly", "Gonzalo Higuain", "Jose Callejon"]
# This class will handles any incoming request from the Xamarin application
# To move to production: This will need a lot of further development into it's security

class Handler(BaseHTTPRequestHandler):

    def is_user(self, user):
        for existing_user in TEST_USERS:
                if existing_user.password == user.password and existing_user.username == user.username: 
                    return existing_user

        return 0

    def json_game_days(self):
        return json.dumps(game_days)


    def handle_login(self, args):
        if args["username"] and args["password"]:
            user = User(args["username"], args["password"])

            # DO WORK HERE:
            # TO CHECK DATABASE IF USER EXISTS
            existing_user = self.is_user(user)

            if existing_user:
                user = get_user(args["username"])
                if not user.picks:
                    print "User picks were empty on login"
                    user.setupEmptyPicks(game_days_list)
                self.send_response(200)
                self.end_headers()
                # Send back User Settings
                self.wfile.write(existing_user.jsonify())
                return 1

        return 0

    def handle_settings_change(self, args):
        if args["username"] and args["password"]:
            user = User(args["username"], args["password"])
            # DO WORK HERE:
            # TO CHECK DATABASE IF USER EXISTS
            existing_user = self.is_user(user)
            # Get/modify User data in DB
            if existing_user: 
                existing_user.updateData(args)
                self.send_response(200)
                self.end_headers()
                # Send back User Settings
                self.wfile.write(existing_user.jsonify())
                return 1

        return 0

    def handle_game_days(self, args):
        if args["username"] and args["password"]:
            user = User(args["username"], args["password"])
            existing_user = self.is_user(user)
            if existing_user:
                self.send_response(200)
                self.end_headers()
                # Send back User Settings
                self.wfile.write(self.json_game_days())
                return 1
        return 0

    def handle_user_picks(self, args):
        if args["username"] and args["password"]:
            user = User(args["username"], args["password"])
            existing_user = self.is_user(user)
            if existing_user:
                self.send_response(200)
                self.end_headers()
                # Send back User's current Player Selctions
                self.wfile.write(existing_user.jsonify_picks())
                return 1
        return 0

    def handle_leagues_on_day(self, args):
        if args["day"]:
            self.send_response(200)
            self.end_headers()
            # Send back Leagues playing on day
            self.wfile.write(json.dumps(leagues))
            return 1
        return 0

    def handle_teams_on_day(self, args):
        if args["day"] and args["league"]:
            self.send_response(200)
            self.end_headers()
            league = urllib2.unquote(args["league"])
            # Send back Teams from a League playing on a day
            self.wfile.write( json.dumps( teams[league] ) )
            return 1
        return 0

    def handle_players_on_day(self, args):
        if args["day"] and args["league"] and args["team"]:
            self.send_response(200)
            self.end_headers()
            team = urllib2.unquote(args["team"])
            # Send back Teams from a League playing on a day
            self.wfile.write( json.dumps( players[team] ) )
            return 1
        return 0

    def handle_player_select(self, args):
        if args["username"] and args["password"]:

            user = User(args["username"], args["password"])
            existing_user = self.is_user(user)

            if existing_user and args["day"] and args["selection_no"] and args["player"]:

                user = get_user(args["username"])
                selection = int(args["selection_no"])

                if args["day"] in game_days_list and selection <= 1:

                    self.send_response(200)
                    self.end_headers()
                    player = urllib2.unquote(args["player"])
                    team = urllib2.unquote(args["team"])
                    league = urllib2.unquote(args["league"])


                    valid_day = False
                    picks = user.picks
                    for date in user.picks:
                        print date + " ?= " + args["day"]
                        if date == args["day"]:
                            print "Found Day"
                            player_list = user.picks[date]
                            player_list[selection] = player
                            picks[date] = player_list
                            valid_day = True
                            break

                    if valid_day:
                        update_player_selections(user, picks)
                        updated_user = get_user(args["username"])
                        # Send back Teams from a League playing on a day
                        self.wfile.write( json.dumps(updated_user.picks) )
                        return 1
        return 0


    def do_GET(self):
        parsed_path = urlparse.urlparse(self.path)

        message_parts = [
                'CLIENT VALUES:',
                'client_address=%s (%s)' % (self.client_address,
                                            self.address_string()),
                'command=%s' % self.command,
                'path=%s' % self.path,
                'real path=%s' % parsed_path.path,
                'query=%s' % parsed_path.query,
                'request_version=%s' % self.request_version,
                '',
                'SERVER VALUES:',
                'server_version=%s' % self.server_version,
                'sys_version=%s' % self.sys_version,
                'protocol_version=%s' % self.protocol_version,
                '',
                'HEADERS RECEIVED:',
                ]
        for name, value in sorted(self.headers.items()):
            message_parts.append('%s=%s' % (name, value.rstrip()))

        success = 0

        if parsed_path.query:

            arg_dict = dict()
            key_vals = parsed_path.query.split("&");
            for key_val in key_vals:
                arg = key_val.split("=")
                arg_dict[arg[0]] = arg[1]

            if "query" in arg_dict.keys():
                query = arg_dict["query"]

                if query == "next_seven_game_days":
                    success = self.handle_game_days(arg_dict)
                elif query == "users_picks":
                    success = self.handle_user_picks(arg_dict)
                elif query == "leagues":
                    success = self.handle_leagues_on_day(arg_dict)
                elif query == "teams":
                    success = self.handle_teams_on_day(arg_dict)
                elif query == "players":
                    success = self.handle_players_on_day(arg_dict)

        if success == 0:
            message_parts.append('')
            message = '\r\n'.join(message_parts)
            self.send_response(400)
            self.end_headers()
            self.wfile.write(message)

        return

    def do_POST(self):
        parsed_path = urlparse.urlparse(self.path)
        # Parse the form data posted
        form = cgi.FieldStorage(
            fp=self.rfile,
            headers=self.headers,
            environ={'REQUEST_METHOD':'POST',
                     'CONTENT_TYPE':self.headers['Content-Type'],
                     })

        # Echo back information about what was posted in the form
        arg_dict = dict()
        if form and form.keys():
            for field in form.keys():
                field_item = form[field]
                arg_dict[field] = form[field].value

        # did we successfully parse the request?
        success = 0
        if len(arg_dict) > 0:
            if "query" in arg_dict.keys():
                query = arg_dict["query"]

                if query == "login":
                    success = self.handle_login(arg_dict)
                elif query == "settings_change":
                    success = self.handle_settings_change(arg_dict)
                elif query == "player_selection":
                    success = self.handle_player_select(arg_dict)

        # If we could not parse the request send a 400 response back
        if success == 0:
            self.send_response(400)
            self.end_headers()

        # What thread was this request served on
        #self.wfile.write( "thread: " + threading.currentThread().getName() )

        return




# Handle each request on separate thread if need be
class ThreadedHTTPServer(ThreadingMixIn, HTTPServer):
    """Handle requests in a separate thread."""




try:
    #Create a web server and define the handler to manage the
    #incoming request
    server = HTTPServer(('localhost', PORT_NUMBER), Handler)
    # server.socket = ssl.wrap_socket (server.socket, certfile="server.pem", server_side=True)
    print 'Started httpserver on port ' , PORT_NUMBER

    #Wait forever for incoming htto requests
    server.serve_forever()

except KeyboardInterrupt:
    print '^C received, shutting down the web server'
    server.socket.close()
